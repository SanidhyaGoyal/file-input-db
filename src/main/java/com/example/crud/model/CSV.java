package com.example.crud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tutorials")
public class CSV {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "published")
    private String published;

    public CSV() {

    }

    public CSV(String id, String title, String description, String published) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.published = published;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String isPublished() {
        return published;
    }

    public void setPublished(String isPublished) {
        this.published = isPublished;
    }

    @Override
    public String toString() {
        return "Csv [id=" + id + ", title=" + title + ", desc=" + description + ", published=" + published + "]";
    }
}