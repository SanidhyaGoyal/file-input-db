package com.example.crud;

import com.example.crud.model.CSV;
import com.example.crud.repository.CSVRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

@Component
public class CSVService implements CommandLineRunner {

    @Autowired
     CSVRepository csvRepository;

    @Override
    public void run(String... args) throws Exception {
        BufferedReader csvReader = new BufferedReader(new FileReader("/Users/consultadd/Desktop/SanidhyaGoyal-springboot-postgres-38dce85048a9/crud/src/main/resources/Input.csv"));


        String row = "";
        String[] data = null;

        ArrayList<String> arr1 = new ArrayList<>();
        CSV csv =new CSV();

        while ((row = csvReader.readLine()) != null) {
            data = row.split(",");
            System.out.println(data[1]+data[0]+data[2]+data[3]);
            csv.setId(data[0]);
            csv.setTitle(data[1]);
            csv.setDescription(data[2]);
            csv.setPublished(data[3]);
            System.out.println(csv);
            csvRepository.save(csv);
        }

    }
}
