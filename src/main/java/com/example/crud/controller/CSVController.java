package com.example.crud.controller;

import com.example.crud.model.CSV;
import com.example.crud.repository.CSVRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/csv")
public class CSVController {

   @Autowired
   private CSVRepository csvRepository;


   @GetMapping("/")
   public List<CSV> getCSV() throws IOException {

      FileWriter fw = new FileWriter("Output.csv");
      fw.write(String.valueOf(csvRepository.findAll()));
      fw.write(csvRepository.toString());
      fw.close();
      return  csvRepository.findAll();
   }

}
