package com.example.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class CrudApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(CrudApplication.class, args);
	}

}
